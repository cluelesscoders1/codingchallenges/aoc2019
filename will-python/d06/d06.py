#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 06
https://adventofcode.com/2019/day/4

"""

import argparse

def generateOrbitMap(orbdata):
	"""Generates the orbit map"""
	return dict(reversed(row.split(")")) for row in orbdata)

def findPath(oribtMap, planet):
	path = []
	for planet in iter(lambda: oribtMap.get(planet), None):
		path.append(planet)
	return path

def part1(fileName):
	"""Part 1 activites"""
	with open(fileName,"r") as datafile:
		data = datafile.read().splitlines()
		orbitMap = generateOrbitMap(data)
		paths = sum(len(findPath(orbitMap, planet)) for planet in orbitMap)

		print(f"Total length: { paths } ")
		return paths

def part2(fileName,targets):
	"""Part 2 activities"""
	with open(fileName,"r") as datafile:
		data = datafile.read().splitlines()
		orbitMap = generateOrbitMap(data)
		path_len = len(set(findPath(orbitMap,target[0])) ^ set(findPath(orbitMap,targets[1])))
		print(f"Part 2 total length: { path_len } ")
		return path_len


def main(parts,fileNames,targets):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		p1val = part1(fileNames[0])
	if parts == 3 or parts == 2:
		p2val = part2(fileNames[1],targets)	

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 06: Santa needs gas')
	parser.add_argument('-p1','--d01p1File', help="Name of the Day 06 part 1 input text file") 
	parser.add_argument('-p2','--d01p2File', help="Name of the Day 06 part 2 input text file") 
	parser.add_argument('-t2','--p2target', help="Name of the Day 06 part 2 target/planet") 
	parser.add_argument('-o2','--p2origin', help="Name of the Day 06 part 2 origin/planet") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d01p1File, args.d01p1File]
	target = [args.p2target, args.p2origin]
	main(args.runpart,infiles,target)
