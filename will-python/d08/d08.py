#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 08
https://adventofcode.com/2019/day/8
"""
import argparse

def ReadInput(filename,width,length):
	"""Reads the input and creates the layers based width and length."""
	with open(filename,"r") as infile:
		data = infile.read()

		layers = []
		pointer = 0
		for layerCount in range(int(len(data)/(width*length))):
			layer = []
			for row in range(length):
				layer.append(data[pointer:pointer+width])
				pointer += width
			layers.append(layer)

		return layers


def PrintLayers(layers,singleLayer=None):
	"""Prints layers, used for debugging."""
	if singleLayer != None:
		layer = layers
		print(f"Layer { singleLayer }: ")
		for row in layer:
			print(f'\t{ row }')
	else:
		for layer in enumerate(layers):
			print(f"Layer { layer[0] }: ")
			for row in layer[1]:
				print(f'\t{ row }')

def part1Analysis(data):
	"""Need to find the layer with fewest 0 digits and multply # of 1's # of 2's found
	This function will have the parameters hard coded."""

	finderNumber = None
	foundLayer = ''
	charToFind = '0' 

	for layer in enumerate(data): 
		foundNumber = 0
		for row in layer[1]:
			foundNumber += row.count(charToFind) 

		# print(f'Layer {layer[0]} foundNumber: {foundNumber}')
		if finderNumber == None or finderNumber > foundNumber:
			foundLayer = layer[0] 
			finderNumber = foundNumber

	print(f'Found layer: {foundLayer} with { finderNumber } found.')
	PrintLayers(data[foundLayer],foundLayer)
	ones = 0 # Count found of # 1
	twos = 0 # Count found of # 2 
	for row in data[foundLayer]:
		ones += row.count('1')
		twos += row.count('2')

	print(f"Result of 1's * 2's is {ones} * {twos} = { ones * twos }")
	return ones * twos

def part2Analysis(layers,width,length):
	viewLayer = [['2'] * width for _ in range(length)]
	for layer in enumerate(layers):
		for row in enumerate(layer[1]):
			for char in enumerate(row[1]): 
				if char[1] == '2': # Transparent, pass
					pass
				elif char[1] == '1' and viewLayer[row[0]][char[0]] == '2': # Set white 
					viewLayer[row[0]][char[0]] = char[1]
				elif char[1] == '0' and viewLayer[row[0]][char[0]] == '2': # Set black
					viewLayer[row[0]][char[0]] = char[1]
				# print(f'Layer: {layer[0]} on {row[0]},{char[0]} with char: {char[1]}')
				# PrintLayers(viewLayer,f'{layer[0]},{row[0]},{char[0]}')
	return viewLayer

def DrawImage(layer):
	"""Draw the image with dots and spaces"""
	for row in layer:
		rowstr = ''
		for char in row:
			if char == '1':
				rowstr += 'X'
			else:
				rowstr += ' '
		print(rowstr)


def part1(filename,layerSize):
	"""Collect layers and find stuff."""
	width,length = layerSize.split(',')	
	data = ReadInput(filename, int(width), int(length))
	# PrintLayers(data)
	return part1Analysis(data)

def part2(filename,layerSize):
	"""Collect filename and layers"""
	width,length = layerSize.split(',')	
	data = ReadInput(filename, int(width), int(length))
	finalImage = part2Analysis(data, int(width), int(length))
	PrintLayers(finalImage, 'Final Image')
	DrawImage(finalImage)





def main(parts,fileNames,layerSize):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		p1val = part1(fileNames[0],layerSize)
		print("Part 1 solution:", str(p1val))
	if parts == 3 or parts == 2:
		p2val = part2(fileNames[0],layerSize)	
		# print("Part 2 solution:", str(p2val))
		pass

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 08: Elf password pictograms')
	parser.add_argument('-p1','--d01p1File', help="Name of the Day08 part 1 input text file") 
	parser.add_argument('-i1','--infopart1', help="Width and length as tuple of layer comma separated.")
	parser.add_argument('-p2','--d01p2File', help="Name of the Day08 part 2 input text file") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d01p1File, args.d01p1File]
	p1data = args.infopart1
	main(args.runpart,infiles,p1data)
