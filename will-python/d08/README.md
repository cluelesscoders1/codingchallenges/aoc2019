# AoC Day 08 2019

Link for the problem: https://adventofcode.com/2019/day/8

Essentially the problem is to take a single string of digits and break it up into width by length layers. Then the problem objectives kick in. 

## Part 1 

Sample data and desired output. The goal is to find the layer with the least number `0`'s in the layer. Then multiple the count of `1`'s found by the number of `2`s. 

Input: `123456789012`

Output: 

```
Layer 1: 123
         456

Layer 2: 789
         012
```

Output from Example:
```
$ python d08.py -r 1 -p1 sample1.txt -i1 '3,2'
Found layer: 0 with 0 found.
Layer 0: 
	123
	456
Result of 1's * 2's is 1 * 1 = 1
Part 1 solution: 1
```

Output from problem set: 
```
$ python d08.py -r 1 -p1 d08p1data.txt -i1 '25,6'
Found layer: 5 with 9 found.
Layer 5: 
	2202222222212222222222221
	1221122212222222222212222
	2222222222222120222201221
	2222222222222220210222112
	2222222202122222222211222
	2022122222220220211222222
Result of 1's * 2's is 19 * 122 = 2318
Part 1 solution: 2318
```

## Part 2

Taking everything from part 1, now we take the layers and try and build an image by layering images. The key for the images are now:

| Value | Color |
|:----- | -----:|
|0      | black |
|1      | white |
|2		| clear |

Essentially, stack the layers replacying clear values through the stacks as you get overlaying "white" and "black" in order read the 
output. 

Very simple overall, create an array of all "clear" and if the viewing image is 2 and the value matched up is not 2, change the viewing image to the respective color. 

Example for this with an input `0222112222120000` creates:

```
Layer 1: 02
         22

Layer 2: 11
         22

Layer 3: 22
         12

Layer 4: 00
         00
```

Final result is: 
```
01
10
```
As you can see, each layer blocks 1 position at a time with a non-two. Once changed from clear, it stays that color/value. Continue until done with layers or out of 2's. 

Example from program. 
```
$ python d08.py -r 2 -p1 sample2.txt -i1 '2,2'
Layer Final Image: 
	['0', '1']
	['1', '0']
 X
X 
```

Problem final output: 
```
$ python d08.py -r 2 -p1 d08p1data.txt -i1 '25,6'
Layer Final Image: 
	['0', '1', '1', '0', '0', '1', '0', '0', '1', '0', '1', '1', '1', '1', '0', '0', '1', '1', '0', '0', '1', '1', '1', '0', '0']
	['1', '0', '0', '1', '0', '1', '0', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0', '1', '0', '1', '0', '0', '1', '0']
	['1', '0', '0', '1', '0', '1', '1', '1', '1', '0', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '1', '1', '1', '0', '0']
	['1', '1', '1', '1', '0', '1', '0', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '0', '0', '1', '0']
	['1', '0', '0', '1', '0', '1', '0', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0', '1', '0', '1', '0', '0', '1', '0']
	['1', '0', '0', '1', '0', '1', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '1', '0', '0']
 XX  X  X XXXX  XX  XXX  
X  X X  X X    X  X X  X 
X  X XXXX XXX  X    XXX  
XXXX X  X X    X    X  X 
X  X X  X X    X  X X  X 
X  X X  X X     XX  XXX  
```
