#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 09
https://adventofcode.com/2019/day/9
"""
import argparse
from intcomputer import computer 
from collections import defaultdict 

def ReadFile(filename):
	"""Reads the file and returns an array of strings comma deliniated"""
	with open(filename,'r') as programfile:  
		programdata = programfile.readlines()
		opcodes = defaultdict(int,{position:int(place) for position,place in enumerate(programdata[0].split(','))}) # computer instructions from text file. 	
		return opcodes

def part1(filename):
	"""Part 1"""
	program = ReadFile(filename)
	numberCruncher = computer(program)
	numberCruncher.run()
	# print(numberCruncher.stack)
	# print(numberCruncher.relativeBase)
	# print(numberCruncher.stack)
	return 0


def main(parts,fileNames):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		p1val = part1(fileNames[0])
		# print("Part 1 solution:", str(p1val))
	if parts == 3 or parts == 2:
		# p2val = part2(fileNames[0],layerSize)	
		# print("Part 2 solution:", str(p2val))
		pass


if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 09: Elf password pictograms')
	parser.add_argument('-p1','--d09p1File', help="Name of the Day09 part 1 input text file") 
	# parser.add_argument('-i1','--infopart1', help="Width and length as tuple of layer comma separated.")
	parser.add_argument('-p2','--d09p2File', help="Name of the Day09 part 2 input text file") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d09p1File, args.d09p1File]
	# p1data = args.infopart1
	main(args.runpart,infiles)