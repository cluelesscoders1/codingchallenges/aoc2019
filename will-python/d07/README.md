# Day 7 - Int Ampifier Computer 

https://adventofcode.com/2019/day/7

This problem gets fun, it takes several instances of the int computer that 
was built previously. The computer wil run a program that takes an input on 
each one of the 5 amplifiers that are 0 through 4.

## Part 1 

Searching for max settings on the amplifiers. The goal is to take the output of 
one 'amp computer' into another. Each amp runs a seperate instance of the int 
computer with 2 inputs. The inputs are the program and the amp setting. Edits
to previous iterations of the code is the input method will pull from a 
parameter passed to the for the "amplifier" value plus the output of the 
previous amp into the next amp. 

There is a role with the amp "phase" settings. Each amp has a unique phase from 
0 to 4 in this part. The first amp starts with an input of zero. 

Example: 
```
$ python d07.py -r 1 -p1 d07p1Data.txt 
Maxval: 17790 Phases: (2, 1, 3, 4, 0)
```

## Part 2 

Everthing we learned in part 1 will continue in part 2 now with a "feedback" loop. 
The amps A-E will be run. Apparently this keeps on going until the amplifier hits a
halt condition. Once the halt condition has been hit, the last value from Amp E is 
then returned as the highest value. 

Major gotcha's I had to look up. Sticking points:
* Each amp will modify it's stack and reuse upon each iteration. 
* When an amp stops, the pointer value must be saved and set on each computer. 
* Output for the signal for the thrusters will ALWAYS be taken from `Amp E`.
* Only input for the amps is phase (first time only) followed by the output of the amp before it. The only input ever given is `Amp A` with a value of `0` by the user. 

This problem took me way too long when I realized my samples for part 2 were not accurate, nor the hints from the line above. 

Sample inputs: 
* Max thruster signal `139629729` (from phase setting sequence `9,8,7,6,5`):
```
3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5
```
* Max thruster signal `18216` (from phase setting sequence `9,7,8,5,6`):
```
3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10
```

> **What is the highest signal that can be sent to the thrusters?**
```
$ python d07.py -r 2 -p1 d07p1Data.txt 
...
...
Max thruster signal: 19384820 
```

## Test code exmples

* Test Sample 4: `python d07.py -r 2 -p1 sample4.txt -i1 '9,8,7,6,5'`
* Test Sample 5: `python -m pdb  d07.py -r 2 -p1 sample5.txt -i1 '9,7,8,5,6'`

Excellent question! 
